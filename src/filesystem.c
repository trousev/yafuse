#include "filesystem.h"
#define FUSE_USE_VERSION 30

#include <fuse/fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#ifndef S_IFDIR
#define S_IFDIR __S_IFDIR
#endif

#ifndef S_IFREG
#define S_IFREG __S_IFREG
#endif



static int m_getattr (const char * path, struct stat *st) {
    printf("getattr call for %s\n", path);
    st->st_uid = getuid();
    st->st_gid = getgid();

    st->st_atime = time(NULL);
    st->st_mtime = time(NULL);

    if(strcmp(path, "/") == 0) {
        st->st_mode = S_IFDIR | 0755;
        st->st_nlink = 2;
    } else {
        st->st_mode = S_IFREG | 0644;
        st->st_nlink = 1;
    }

    return 0;
}


static int m_readdir(const char * path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {
    printf("readdir call for %s\n",path);
    filler(buffer, ".", NULL, 0);
    filler(buffer, "..", NULL, 0);
    if(strcmp(path, "/") == 0) {
        filler(buffer, "hello.txt", NULL, 0);
        filler(buffer, "world.txt", NULL, 0);
    }
    return 0;
}
static int *m_read (const char * path, char * buffer, size_t size, off_t offset, struct fuse_file_info * fi) {
    printf("read call for %s %d:%d\n", path, offset, size+offset);
    
    char * text = "Unknown";
    if(strcmp(path, "/hello.txt") == 0) {
        text = "Hello";
    }
    if(strcmp(path, "/world.txt") == 0) {
        text = "World";
    }

    int result_size = strlen(text) - offset;
    if(result_size <= 0) return 0;
    if(result_size > size) result_size = size;
    memcpy(buffer, text+offset, result_size);
    return result_size;
}

static struct fuse_operations operations = {
    .getattr = m_getattr,
    .readdir = m_readdir,
    .read = m_read    
};


void filesystem_start(int q, char*argv[]) {
    fuse_main(q, argv, &operations, NULL);
}